package com.rabbitmq.changchun;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class 生产者_队列 {
	static String TASK_QUEUE_NAME = "test";

	public static void main(String[] argv) {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("10.20.0.20");
			factory.setUsername("guest");
			factory.setPassword("guest");
//			factory.setVirtualHost("jyt");
			factory.setPort(9678);
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			boolean durable = true;// 消息持久化，如果服务器挂了，不会丢失
			channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);
			// 分发消息
			for (int i = 0; i < 2; i++) {
				String message = "Hello World!xxxxxxxxxxxx " + i;
				channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
				System.out.println(" [x] Sent '" + message + "'");
				Thread.sleep(400);
			}
			Thread.sleep(400);
			channel.close();
			connection.close();
		} catch (Exception e) {
			System.out.println(e.getClass() + ",异常：" + e.getMessage());
		}
	}
}