package com.rabbitmq.mq2.工作队列;
import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class NewTask {

    private static final String TASK_QUEUE_NAME = "task_queue";

    public static void main(String[] argv) throws java.io.IOException, Exception {
        ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        boolean durable = true;//消息持久化，如果服务器挂了，不会丢失
        channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);
//      分发消息
        for(int i = 0 ; i < 2; i++){
            String message = "Hello World!xxxxxxxxxxxx " + i;
            channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
            Thread.sleep(400);
        }
        Thread.sleep(400);
        channel.close();
        connection.close();
    }
}