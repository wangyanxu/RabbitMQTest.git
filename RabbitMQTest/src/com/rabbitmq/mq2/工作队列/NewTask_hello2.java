package com.rabbitmq.mq2.工作队列;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class NewTask_hello2 {

	private static final String TASK_QUEUE_NAME = "hello2";

	public static void main(String[] argv) throws java.io.IOException, Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		boolean durable = true;// 消息持久化，如果服务器挂了，不会丢失
		channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);
		// 分发消息
//		AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties.Builder();
//		builder.expiration("3000000");
        com.rabbitmq.client.AMQP.BasicProperties bp = new AMQP.BasicProperties.Builder()
//        		.headers(headers)
        		.expiration("30000")//设置了一个过期时间
        		.deliveryMode(2)
        		.build();
		for (int i = 0; i < 5; i++) {
			String message = "hw22222222222" + i;
			channel.basicPublish("", TASK_QUEUE_NAME, bp, message.getBytes());
			System.out.println(" [x] Sent '" + message + "'");
			Thread.sleep(400);
		}
		Thread.sleep(400);
		channel.close();
		connection.close();
	}
}