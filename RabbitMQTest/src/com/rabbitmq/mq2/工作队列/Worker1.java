package com.rabbitmq.mq2.工作队列;

import java.io.IOException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Worker1 {
	private static final String TASK_QUEUE_NAME = "123";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		final Connection connection = factory.newConnection("test");
		final Channel channel = connection.createChannel();
		channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		System.out.println("Worker1 [*] Waiting for messages. To exit press CTRL+C");
		// 每次从队列中获取数量
		channel.basicQos(1);//不要一次将多个消息发送给一个消费者，只有当消费者处理完成当前消息并反馈后，才会收到另外一条消息或任务。这样就避免了负载不均衡的事情
		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println("Worker1 [x] Received '" + message + "'");
				try {
					doWork(message);
				} finally {
					System.out.println("Worker1 [x] Done");
					// 消息处理完成确认
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}
		};
		// 消息消费完成确认
		channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
	}

	private static void doWork(String task) {
		try {
			Thread.sleep(2000); // 暂停1秒钟
		} catch (InterruptedException _ignored) {
			Thread.currentThread().interrupt();
		}
	}
}