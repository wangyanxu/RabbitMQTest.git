package com.rabbitmq.mq2.工作队列;

import java.io.IOException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class 多消费者 {

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		factory.setVirtualHost("pad");
		for (int i = 1000; i < 9000; i++) {
			new Thread(new WThread(factory, i + "")).start();
		}
	}
}

class WThread implements Runnable {
	private static String TASK_QUEUE_NAME = "task_queue_test";
	ConnectionFactory factory;
	String connName;

	public WThread(ConnectionFactory factory, String connName) {
		this.factory = factory;
		this.connName = connName;
	}

	@Override
	public void run() {
		try {
//			Address[] addrs= {new Address("test" + connName)};
//			final Connection connection = factory.newConnection(addrs, "test_p_" + connName);
			final Connection connection = factory.newConnection("test" + connName);
			final Channel channel = connection.createChannel();
			channel.queueDeclare(TASK_QUEUE_NAME+"_"+connName, true, false, false, null);
			System.out.println("Worker2 [*] Waiting for messages. To exit press CTRL+C");
			// 每次从队列中获取数量
			channel.basicQos(1);
			final Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println("Worker2 [x] Received '" + message + "'");
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			};
			// 消息消费完成确认
			channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}