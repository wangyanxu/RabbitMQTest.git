package com.rabbitmq.mq3.发布订阅者;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class EmitLog定义队列不在线也可以接收 {

	private static final String EXCHANGE_NAME = "test_exchange_fanout_0116123";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT,true);
		// 分发消息
		for (int i = 0; i < 5; i++) {
			String message = "test_exchange_fanout_0116123 World! " + i;
			channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
			System.out.println(" [x] Sent '" + message + "'");
		}
		channel.close();
		connection.close();
	}
}