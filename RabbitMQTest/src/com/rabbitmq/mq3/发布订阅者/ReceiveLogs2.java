package com.rabbitmq.mq3.发布订阅者;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.*;

import java.io.IOException;

public class ReceiveLogs2 {
	private static final String EXCHANGE_NAME = "test_exchange_fanout_0116123";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, "fanout",true);
		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, EXCHANGE_NAME, "");
		System.out.println(" [*2] Waiting for messages. To exit press CTRL+C");
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println(" [x2] Received '" + message + "'");
			}
		};
		channel.basicConsume(queueName, true, consumer);
	}
}