package com.rabbitmq.mq3.发布订阅者;

import java.io.IOException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer;

public class Recv定义队列不在线也可以接收 {

	private final static String QUEUE_NAME = "test_queue_fanout_1";

	private final static String EXCHANGE_NAME = "test_exchange_fanout_0116";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		// 声明队列
		// channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		// 绑定队列到交换机
		channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");
		// 同一时刻服务器只会发一条消息给消费者
		channel.basicQos(1);
		// 定义队列的消费者
		DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x1] Received '" + message + "'");
            }
        };
		// 监听队列，手动返回完成
		channel.basicConsume(QUEUE_NAME, true, consumer);
		
	}
}