package com.rabbitmq.mq4.routing;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.*;

import java.io.IOException;

public class ReceiveLogsDirect4 {
	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		channelRoute(connection);
	}


	public static void channelRoute(Connection connection) throws IOException {
		System.out.println("---定义路由:" );
		Channel channel = connection.createChannel(3);
		// 声明交换器
		channel.exchangeDeclare("test2", "direct");
		// 获取匿名队列名称
		String queueName = channel.queueDeclare().getQueue();
		// 根据路由关键字进行多重绑定
		channel.queueBind(queueName, "test2", "rp");
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println("路由接收:" + ",接收:" + message);
			}
		};
		channel.basicConsume(queueName, true, consumer);
	}
}