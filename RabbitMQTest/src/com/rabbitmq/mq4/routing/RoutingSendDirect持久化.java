package com.rabbitmq.mq4.routing;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

public class RoutingSendDirect持久化 {

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		// 声明交换器
		channel.exchangeDeclare("test2", BuiltinExchangeType.DIRECT,false);
		// 发送消息
		String message = "Send the message level:test";
		channel.basicPublish("test2", "rp", null, message.getBytes());
		channel.close();
		connection.close();
	}
}