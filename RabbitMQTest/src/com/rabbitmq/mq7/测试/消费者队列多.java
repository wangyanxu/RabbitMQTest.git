package com.rabbitmq.mq7.测试;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class 消费者队列多 {
	private static final String TASK_QUEUE_NAME = "jyt_queue0";
	private static List<String> list = new ArrayList<>();

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		final Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		receive(channel);
		int i = 0;
		while (true) {
			Iterator<String> it = list.iterator();
			while(it.hasNext()){
				System.out.println(it.next());
				it.remove();
			}
			i++;
			System.out.println("第"+i+"次完成");
			Thread.sleep(3000);
		}
	}

	public static void receive(final Channel channel) throws IOException, TimeoutException, InterruptedException {
		channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		channel.basicQos(1);// 不要一次将多个消息发送给一个消费者，只有当消费者处理完成当前消息并反馈后，才会收到另外一条消息或任务。这样就避免了负载不均衡的事情
		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
//				System.out.print(properties.getHeaders());
				String message = new String(body, "UTF-8");
//				System.out.println(",接收:" + message);
				list.add(message);
				channel.basicAck(envelope.getDeliveryTag(), false);// 消息处理完成确认
			}
		};
		// 消息消费完成确认
		channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
	}

}