package com.rabbitmq.mq7.测试;
import java.util.HashMap;
import java.util.Map;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class 生产者队列 {

    private static final String TASK_QUEUE_NAME = "task_queue_test_5999";
	
    public static void main(String[] argv) throws java.io.IOException, Exception {
        ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		factory.setVirtualHost("pad");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        boolean durable = true;//消息持久化，如果服务器挂了，不会丢失
        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("vhost", "/");
//        map.put("username","guest");
//        map.put("password", "guest");
//        map.put("x-message-ttl", 60000);  //过期时间，毫秒
        map.put("x-expires", 3000000);  //微妙,1000*1000*30,队列的消息过期
//        map.put("x-message-ttl", 60000); 
        channel.queueDeclare(TASK_QUEUE_NAME, durable, false, false, null);
//      分发消息
//        Map<String, Object> headers = new HashMap<String, Object>();
//        headers.put("name",  "头目");
//        headers.put("age", 28);
        BasicProperties bp = new AMQP.BasicProperties.Builder()
//        		.headers(headers)
        		//.expiration("30000")//设置了一个过期时间
        		.deliveryMode(2)
        		.build();
        for(int i = 100 ; i < 101; i++){
            String message = "我是生产者发送过来的消息:" + i;
            channel.basicPublish("", TASK_QUEUE_NAME, bp, message.getBytes());
            System.out.println("Sent " + message);
        }
        channel.close();
        connection.close();
    }
}