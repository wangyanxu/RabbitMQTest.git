package com.rabbitmq.mq7.测试;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class 生产者队列多 {

    private static final String TASK_QUEUE_NAME = "wyx_";
	
    public static void main(String[] argv) throws java.io.IOException, Exception {
        ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
        Connection connection = factory.newConnection();
        publish(connection.createChannel());
        connection.close();
    }
    
    public static void publish(Channel channel) throws IOException, TimeoutException{
        boolean durable = true;//消息持久化，如果服务器挂了，不会丢失
        for(int i = 1000 ; i <2000; i++){
            channel.queueDeclare(TASK_QUEUE_NAME+i, durable, false, false, null);
//            String message = "我是生产者发送过来的消息!, " + i;
//            channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
//            System.out.println("Sent " + message);
        }
        channel.close();
    }
    
}