package com.rabbitmq.mq8.测试;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class n发布 {

	private static final String EXCHANGE_NAME = "test";

	public static void main(String[] argv) throws java.io.IOException, Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		route(connection);
		connection.close();
	}

	private static void route(Connection connection) throws IOException, TimeoutException {
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		// 分发消息
		for (int i = 0; i < 1; i++) {
			String message = "Hello World! xxx" + i;
			channel.basicPublish(EXCHANGE_NAME, "route.pad", null, message.getBytes());
			System.out.println("发送了" + message);
		}
		channel.close();
	}
}
