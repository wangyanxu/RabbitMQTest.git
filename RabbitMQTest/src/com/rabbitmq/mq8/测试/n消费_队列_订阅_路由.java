package com.rabbitmq.mq8.测试;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

/**
 * 
 * @description 订阅，必须在线才可收到，不在线时，消息忽略
 */
public class n消费_队列_订阅_路由 {

	private static final String QUEUE_NAME = "queue";
	private static final String EXCHANGE_NAME = "test2";
	private static final String ROUTE_NAME = "route";

	public static void main(String[] argv) throws java.io.IOException, Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		fanout(factory);
	}

	// 订阅接收
	private static void fanout(ConnectionFactory factory) throws IOException, TimeoutException {
		for (int i = 0; i < 2; i++) {
			String connName = null;
			String routeName = null;
			if (i % 2 == 0) {
				connName = "pad_0" + i;
				routeName = ".pad";
			} else {
				connName = "box_0" + i;
				routeName = ".box";
			}
			Connection connection = factory.newConnection(connName);
//			new Thread(new CQueue2Thread(connName, connection, QUEUE_NAME)).start();
//			new Thread(new CRouteThread(connName, connection, EXCHANGE_NAME, ROUTE_NAME + routeName, QUEUE_NAME)).start();
			if (i % 2 == 0) {
				new Thread(new CSubThread(connName, connection, EXCHANGE_NAME, ROUTE_NAME + routeName, QUEUE_NAME)).start();
			}
		}
	}
}

class CQueue2Thread implements Runnable {
	String queueName;
	Connection connection;
	String connName;

	public CQueue2Thread(String connName, Connection connection, String queueName) {
		this.queueName = queueName;
		this.connection = connection;
		this.connName = connName;
	}

	@Override
	public void run() {
		channelQueue();
	}

	public void channelQueue() {
		System.out.println("---" + connName + "定义队列:" + queueName);
		final Channel channel;
		try {
			channel = connection.createChannel(1);
			channel.queueDeclare(queueName, true, false, false, null);
			// 每次从队列中获取数量
			channel.basicQos(1);// 不要一次将多个消息发送给一个消费者，只有当消费者处理完成当前消息并反馈后，才会收到另外一条消息或任务。这样就避免了负载不均衡的事情
			final Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println(connName + "," + queueName + ",接收：" + message);
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			};
			// 消息消费完成确认
			channel.basicConsume(queueName, false, consumer);
		} catch (IOException e) {
		}

	}

}

class CSubThread implements Runnable {
	String queueName;
	Connection connection;
	String connName;
	String exchangename;
	String routeName;

	public CSubThread(String connName, Connection connection, String exchangename, String routeName, String queueName) {
		this.queueName = queueName;
		this.connection = connection;
		this.routeName = routeName;
		this.exchangename = exchangename;
		this.connName = connName;
	}

	@Override
	public void run() {
		channelSub();
	}

	public void channelSub() {
		System.out.println("---" + connName + "定义订阅:"+routeName);
		final Channel channel;
		try {
			channel = connection.createChannel();
			channel.exchangeDeclare("test", "fanout");
			String qName = channel.queueDeclare().getQueue();
			channel.queueBind(qName, "test", routeName);
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println(connName + "订阅接收：" + message);
				}
			};
			channel.basicConsume(qName, true, consumer);
		} catch (IOException e) {
		}
	}
}

class CRouteThread implements Runnable {
	String queueName;
	Connection connection;
	String connName;
	String exchangename;
	String routeName;

	public CRouteThread(String connName, Connection connection, String exchangename, String routeName,
			String queueName) {
		this.queueName = queueName;
		this.connection = connection;
		this.routeName = routeName;
		this.exchangename = exchangename;
		this.connName = connName;
	}

	@Override
	public void run() {
		channelRoute();
	}

	public void channelRoute() {
		try {
			Channel channel = connection.createChannel();
			channel.exchangeDeclare("test2", "direct");
			String qName = channel.queueDeclare().getQueue();
			channel.queueBind(qName, "test2", "rp");
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println("路由接收:" + ",接收:" + message);
				}
			};
			channel.basicConsume(qName, true, consumer);
		} catch (IOException e) {
			System.out.println("路由IOException:" + e.getMessage());
		}
	}
}