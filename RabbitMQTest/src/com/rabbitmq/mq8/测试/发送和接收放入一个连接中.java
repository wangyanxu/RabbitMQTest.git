package com.rabbitmq.mq8.测试;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;

public class 发送和接收放入一个连接中 {

	private static final String TASK_QUEUE_NAME = "task_queue";

	public static void main(String[] argv) throws java.io.IOException, Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection("我是自定义连接名称");
		new Thread(new PThread(connection, TASK_QUEUE_NAME)).start();
		new Thread(new CThread(connection, TASK_QUEUE_NAME)).start();
	}

}

class CThread implements Runnable {
	String queueName;
	Connection connection;

	public CThread(Connection connection, String queueName) {
		this.queueName = queueName;
		this.connection = connection;
	}

	@Override
	public void run() {
		final Channel channel;
		try {
			channel = connection.createChannel();
			channel.queueDeclare(queueName, true, false, false, null);
			// 每次从队列中获取数量
			channel.basicQos(1);// 不要一次将多个消息发送给一个消费者，只有当消费者处理完成当前消息并反馈后，才会收到另外一条消息或任务。这样就避免了负载不均衡的事情
			final Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println("接收：" + message);
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			};
			// 消息消费完成确认
			channel.basicConsume(queueName, false, consumer);
		} catch (IOException e) {
		}
	}

}

class PThread implements Runnable {
	String queueName;
	Connection connection;

	public PThread(Connection connection, String queueName) {
		this.queueName = queueName;
		this.connection = connection;
	}

	@Override
	public void run() {
		Channel channel = null;
		try {
			channel = connection.createChannel();
			boolean durable = true;// 消息持久化，如果服务器挂了，不会丢失
			channel.queueDeclare(queueName, durable, false, false, null);
			// 分发消息
			for (int i = 0; i < 5; i++) {
				String message = "发送了" + i;
				channel.basicPublish("", queueName, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
				System.out.println("发送了" + message + "'");
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (channel != null) {
				try {
					channel.close();
				} catch (Exception e) {
				}
			}
		}
	}

}