package com.rabbitmq.mq9.json;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class RabbitRestToolConn {

	static String conn = "/api/connections";
	// http://192.168.163.221:15672/api/connections?page=1&page_size=100&name=&use_regex=false
	public static void main(String[] args) throws IOException {
		HttpHost host = new HttpHost("192.168.163.221", 15672);
		// HttpGet httpGet = new
		// HttpGet(conn+"?page=1&page_size=500&name=&use_regex=false&pagination=true");//+"?name='*hello*'"
		HttpGet httpGet = new HttpGet(conn + "?name=&use_regex=false");// +"?name='*hello*'"
		// 认证
		BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope("192.168.163.221", 15672),
				new UsernamePasswordCredentials("guest", "guest"));
		CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
		long time = System.currentTimeMillis();
		HttpResponse response = client.execute(host, httpGet);
		System.out.println("状态:" + response.getStatusLine());
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);
//		System.out.println(result);
		JSONArray ja = JSONArray.fromObject(result);
//		System.out.println(ja.size());
		for (int i = 0; i < ja.size(); i++) {
			Object obj = ja.get(i);
			JSONObject jo = JSONObject.fromObject(obj);
			JSONObject jo2 = JSONObject.fromObject(jo.get("client_properties"));
			Object jo3 = jo2.get("connection_name");
			System.out.print(jo3);
			if(i%100==0){
				System.out.println();
			}else{
				System.out.print(",");
			}
		}
		System.out.println();
		System.out.println("耗时"+(System.currentTimeMillis()-time));
	}
}
