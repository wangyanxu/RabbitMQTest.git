package com.rabbitmq.mq9.json;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class RabbitRestToolQueues {

	static String queues = "/api/queues";
//http://192.168.163.221:15672/api/queues?page=1&page_size=100&name=test&use_regex=false&pagination=true
	public static void main(String[] args) throws IOException {
		HttpHost host = new HttpHost("192.168.163.221", 15672);
		HttpGet httpGet = new HttpGet(queues+"?name=test_quene&consumers=1&use_regex=false");//+"?name='*hello*'"
		// 认证
		BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope("192.168.163.221", 15672),
				new UsernamePasswordCredentials("guest", "guest"));
		CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
		HttpResponse response = client.execute(host, httpGet);
		System.out.println("状态:" + response.getStatusLine());
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);
		System.out.println(result);
//		JSONArray ja = JSONArray.fromObject(result);
//		System.out.println(ja.size());
		// conn
		// for (int i = 0; i < ja.size(); i++) {
		// Object obj = ja.get(i);
		// JSONObject jo = JSONObject.fromObject(obj);
		// JSONObject jo2 = JSONObject.fromObject(jo.get("client_properties"));
		// Object jo3 = jo2.get("connection_name");
		// System.out.println(jo3);
		// }
	}
	// [
	// {
	// "reductions_details": {
	// "rate": 0
	// },
	// "reductions": 3688,
	// "recv_oct_details": {
	// "rate": 0
	// },
	// "recv_oct": 459,
	// "send_oct_details": {
	// "rate": 0
	// },
	// "send_oct": 533,
	// "connected_at": 1515624918482,
	// "client_properties": {
	// "product": "RabbitMQ",
	// "platform": "Java",
	// "information": "Licensed under the MPL. See http://www.rabbitmq.com/",
	// "connection_name": "test1",
	// "copyright": "Copyright (c) 2007-2016 Pivotal Software, Inc.",
	// "capabilities": {
	// "exchange_exchange_bindings": true,
	// "authentication_failure_close": true,
	// "consumer_cancel_notify": true,
	// "basic.nack": true,
	// "publisher_confirms": true,
	// "connection.blocked": true
	// },
	// "version": "3.6.6"
	// },
	// "channel_max": 0,
	// "frame_max": 131072,
	// "timeout": 60,
	// "vhost": "/",
	// "user": "guest",
	// "protocol": "AMQP 0-9-1",
	// "ssl_hash": null,
	// "ssl_cipher": null,
	// "ssl_key_exchange": null,
	// "ssl_protocol": null,
	// "auth_mechanism": "PLAIN",
	// "peer_cert_validity": null,
	// "peer_cert_issuer": null,
	// "peer_cert_subject": null,
	// "ssl": false,
	// "peer_host": "192.168.163.142",
	// "host": "192.168.163.221",
	// "peer_port": 31003,
	// "port": 5672,
	// "name": "192.168.163.142:31003 -> 192.168.163.221:5672",
	// "node": "rabbit@jytpc-02",
	// "type": "network",
	// "garbage_collection": {
	// "minor_gcs": 5,
	// "fullsweep_after": 65535,
	// "min_heap_size": 233,
	// "min_bin_vheap_size": 46422
	// },
	// "channels": 0,
	// "state": "running",
	// "send_pend": 0,
	// "send_cnt": 3,
	// "recv_cnt": 4
	// }
	// ]
}
