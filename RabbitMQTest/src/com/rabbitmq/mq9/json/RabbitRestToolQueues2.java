package com.rabbitmq.mq9.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONObject;

public class RabbitRestToolQueues2 {

	static String queues = "/api/queues/";

	// http://192.168.163.221:15672/api/queues?page=1&page_size=100&name=test&use_regex=false&pagination=true
	public static void main(String[] args) throws IOException {
		List<String> queuelist = new ArrayList<>();
		for(int i=1000;i<1200;i++){
			System.out.println("pad_"+i+"："+getQueue("%2F","pad_"+i));
		}
	}
	public static boolean getQueue(String exchange,String que){
		try {
			HttpHost host = new HttpHost("192.168.163.221", 15672);
			HttpGet httpGet = new HttpGet(queues +exchange+ "/"+que);
			BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
			credentialsProvider.setCredentials(new AuthScope("192.168.163.221", 15672), new UsernamePasswordCredentials("guest", "guest"));
			CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
			HttpResponse response = client.execute(host, httpGet);
			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				String result = EntityUtils.toString(entity);
//				System.out.println(result);
				JSONObject json = JSONObject.fromObject(result);
				Object obj = json.get("consumers");
//				System.out.println("消费者数量："+Integer.valueOf(obj.toString()));
				if(Integer.valueOf(obj.toString()) >0){
					return true;
				}
			} else {
				System.out.println("状态:" + response.getStatusLine());
			}
		} catch (Exception e) {
			System.out.println("异常："+e.getMessage());
		}
		return false;
	}
}
