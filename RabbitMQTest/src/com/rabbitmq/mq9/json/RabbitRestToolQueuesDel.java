package com.rabbitmq.mq9.json;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class RabbitRestToolQueuesDel {

	static String queues = "/api/queues/%2F/";

	// http://192.168.163.221:15672/api/queues?page=1&page_size=100&name=test&use_regex=false&pagination=true
	public static void main(String[] args) throws IOException {
		  for(int i = 1000 ; i<2000; i++){
			  del("pad_"+i);
		}
	}
	public static void del(String str) throws ClientProtocolException, IOException{
		HttpHost host = new HttpHost("192.168.163.225", 15672);
		HttpDelete httpGet = new HttpDelete(queues + str);
		// 认证
		BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope("192.168.163.225", 15672),
				new UsernamePasswordCredentials("guest", "guest"));
		CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
		HttpResponse response = client.execute(host, httpGet);
		if (response.getStatusLine().getStatusCode() == 200) {
			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(entity);
			System.out.println(result);
//			JSONObject json = JSONObject.fromObject(result);
//			Object obj = json.get("consumers");
//			System.out.println("消费者数量："+Integer.valueOf(obj.toString()));
		} else {
			System.out.println(str+",状态:" + response.getStatusLine());
		}
	}
}
