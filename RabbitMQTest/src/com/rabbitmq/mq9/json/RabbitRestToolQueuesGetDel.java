package com.rabbitmq.mq9.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class RabbitRestToolQueuesGetDel {

	static String queues = "/api/queues";

	// http://192.168.163.221:15672/api/queues?page=1&page_size=100&name=test&use_regex=false&pagination=true
	public static void main(String[] args) throws IOException {
		HttpHost host = new HttpHost("192.168.163.225", 15672);
		HttpGet httpGet = new HttpGet(queues + "?name=test_quene&consumers=1&use_regex=false");// +"?name='*hello*'"
		// 认证
		BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope("192.168.163.225", 15672),
				new UsernamePasswordCredentials("guest", "guest"));
		CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
		HttpResponse response = client.execute(host, httpGet);
		System.out.println("状态:" + response.getStatusLine());
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);
		JSONArray ja = JSONArray.fromObject(result);
		System.out.println(ja.size());
		List<String> queueList = new ArrayList<>();
		for (int i = 0; i < ja.size(); i++) {
			Object obj = ja.get(i);
			JSONObject jo = JSONObject.fromObject(obj);
			Object jo3 = jo.get("name");
			System.out.println(jo3);
			queueList.add(jo3.toString());
		}
		for(String queue:queueList){
			if(queue.indexOf("pad_2")>1){
				del(queue);
			}
		}
	}
	public static void del(String str) throws ClientProtocolException, IOException{
		HttpHost host = new HttpHost("192.168.163.225", 15672);
		HttpDelete httpGet = new HttpDelete(queues +"/%2F/"+str);
		// 认证
		BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope("192.168.163.225", 15672),
				new UsernamePasswordCredentials("guest", "guest"));
		CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
		HttpResponse response = client.execute(host, httpGet);
		if (response.getStatusLine().getStatusCode() == 200) {
			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(entity);
			System.out.println(result);
//			JSONObject json = JSONObject.fromObject(result);
//			Object obj = json.get("consumers");
//			System.out.println("消费者数量："+Integer.valueOf(obj.toString()));
		} else {
			System.out.println("状态:" + response.getStatusLine());
		}
	}
}
