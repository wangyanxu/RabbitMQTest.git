package com.rabbitmq.mq91.n队列;

import java.io.IOException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class 多消费者nginx {

	public static void main(String[] argv) throws Exception {
		for (int i = 1000; i < 1001; i++) {
			new Thread(new WThread3(i + "", "test")).start();
		}
	}
}

class WThread3 implements Runnable {
	String connName;
	String vhost;

	public WThread3(String connName, String vhost) {
		this.connName = connName;
		this.vhost = vhost;
	}

	@Override
	public void run() {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("192.168.163.225");
			//factory.setVirtualHost(vhost);
			factory.setPort(5672);
			final Connection connection = factory.newConnection(vhost + "_" + connName);
			final Channel channel = connection.createChannel();
			channel.queueDeclare(vhost + "_" + connName, true, false, false, null);
			System.out.println("Worker2 [*] Waiting for messages. To exit press CTRL+C");
			// 每次从队列中获取数量
			channel.basicQos(1);
			final Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println(vhost + "_" + connName+": '" + message + "'");
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			};
			// 消息消费完成确认
			channel.basicConsume(vhost + "_" + connName, false, consumer);
		} catch (Exception e) {
			System.out.println(e.getCause()+","+e.getMessage());
		}
	}

}