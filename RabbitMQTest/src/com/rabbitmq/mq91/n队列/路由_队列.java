package com.rabbitmq.mq91.n队列;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.*;

import java.io.IOException;

public class 路由_队列 {
	// 交换器名称
	private static final String EXCHANGE_NAME = "wyx123";
	private static final String queueName = "task_queue_test";
	private static final String routeName = "pad";
	// 路由关键字

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		factory.setVirtualHost("pad");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.queueDeclare(queueName, true, false, false, null);
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
		channel.queueBind(queueName, EXCHANGE_NAME, routeName);
		channel.basicPublish(EXCHANGE_NAME, routeName, null, "测水电费".getBytes());
		channel.close();
		connection.close();
	}
}