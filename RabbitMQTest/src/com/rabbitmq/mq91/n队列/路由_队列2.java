package com.rabbitmq.mq91.n队列;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.*;

import java.io.IOException;

public class 路由_队列2 {
	// 交换器名称
	private static final String EXCHANGE_NAME = "wyx1234";
	private static final String routeName = "pad";
	// 路由关键字

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		factory.setVirtualHost("test");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		for (int i = 1000; i < 1006; i++) {
			channel.queueDeclare("venue1_"+i, true, false, false, null);
			channel.queueBind("venue1_"+i, EXCHANGE_NAME, routeName);
		}
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
		com.rabbitmq.client.AMQP.BasicProperties bp = new AMQP.BasicProperties.Builder()// .headers(headers)//头可以设置参数
				.expiration(String.valueOf(30000))// 设置了一个过期时间
				.build();
		channel.basicPublish(EXCHANGE_NAME, routeName, bp, "测试队列1".getBytes());
		for (int i = 1000; i < 1006; i++) {
			channel.queueUnbind("venue1_"+i, EXCHANGE_NAME, routeName);
		}
		channel.close();
		connection.close();
	}
}