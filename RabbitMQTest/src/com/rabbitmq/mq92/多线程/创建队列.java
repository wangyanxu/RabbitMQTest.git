package com.rabbitmq.mq92.多线程;

import java.io.IOException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class 创建队列 {

	private static final String QUEUE_NAME = "2task_more_";

	public static void main(String[] argv) throws java.io.IOException, Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		Connection connection = factory.newConnection();
		queue(connection);
		connection.close();
	}

	private static void queue(Connection connection) {
		Channel channel = null;
		try {
			channel = connection.createChannel();
			boolean durable = true;// 消息持久化，如果服务器挂了，不会丢失
			for(int i=1000;i<1001;i++){
				channel.queueDeclare(QUEUE_NAME+i, durable, false, false, null);
				channel.basicPublish("", QUEUE_NAME+i, MessageProperties.PERSISTENT_TEXT_PLAIN, (QUEUE_NAME+i).getBytes());
			}
			// 分发消息
//			for (int i = 0; i < 1; i++) {
//				String message = "发送了000" + i;
//				channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
//				System.out.println(message);
//			}
		} catch (IOException e) {
		} finally {
			if (channel != null) {
				try {
					channel.close();
				} catch (Exception e) {
				}
			}
		}
	}

}
