package com.rabbitmq.mq92.多线程;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class 创建队列单个并放入消息 {

	private static final String QUEUE_NAME = "9678_test";

	public static void main(String[] argv) {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		factory.setPort(ServerInfo.PORT);
//		factory.setVirtualHost("/");
		Connection connection = null;
		Channel channel = null;
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			boolean durable = true;// 消息持久化
			if(!checkQueue(connection, QUEUE_NAME)){
				channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
			}
			channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, "建立队列了，并放入消息".getBytes("UTF-8"));
//			channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, QUEUE_NAME.getBytes());
		} catch (Exception e) {
			System.out.println("---------------");
			e.printStackTrace();
			System.out.println("" + e.getMessage());
		} finally {
			if (channel != null) {
				try {
					channel.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean checkQueue(Connection conn, String queue){
		try {
			Channel channel = conn.createChannel();
			channel.queueDeclarePassive(queue);
			try {
				channel.close();
			} catch (TimeoutException e) {
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}
}
