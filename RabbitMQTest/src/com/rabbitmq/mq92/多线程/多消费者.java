package com.rabbitmq.mq92.多线程;

import java.io.IOException;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class 多消费者 {

	static String TASK_QUEUE_NAME = "9678_test";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ServerInfo.HOST);
		factory.setPort(ServerInfo.PORT);
		Connection connection = factory.newConnection();
		final Channel channel = connection.createChannel();
		channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		channel.basicQos(1);
		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println("Worker2 [x] Received '" + message + "'");
				channel.basicAck(envelope.getDeliveryTag(), false);
			}
		};
		// 消息消费完成确认
		channel.basicConsume(TASK_QUEUE_NAME, true, consumer);
	}
}
