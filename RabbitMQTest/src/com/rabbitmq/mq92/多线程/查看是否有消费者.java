package com.rabbitmq.mq92.多线程;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.*;
import java.io.IOException;

/**
 * 消息消费者
 */
public class 查看是否有消费者 {

	public static void main(String[] argv) throws Exception {
		// 创建连接工厂
		ConnectionFactory factory = new ConnectionFactory();
		// 设置RabbitMQ地址
		factory.setHost(ServerInfo.HOST);
		factory.setPort(ServerInfo.PORT);
		// 创建一个新的连接
		Connection connection = factory.newConnection();
		// 创建一个频道
		Channel channel = connection.createChannel();
		String QUEUE_NAME = "pad_402823a66240fabb0162410138bb0003";
		// channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		System.out.println("消费者数量:" + channel.consumerCount(QUEUE_NAME));
		channel.close();
		connection.close();
	}
}