package com.rabbitmq.mq92.多线程;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONObject;

public class 测并发http {

	public static void main(String[] args) throws IOException {
		String QUEUE_NAME = "task_more_";
		int threadCount =1;//并发数
		int questCount = 300;//每个线程请求的数量
		CountDownLatch cdl = new CountDownLatch(threadCount);
		for (int i = 0; i < threadCount; i++) {
			new Thread(new MyThreadQueue(cdl, QUEUE_NAME,questCount)).start();
			cdl.countDown();
		}
	}

}

class MyThreadQueue implements Runnable {
	CountDownLatch cdl;
	String QUEUE_NAME;
	int questCount;
	public MyThreadQueue(CountDownLatch cdl, String QUEUE_NAME,int questCount) {
		this.cdl = cdl;
		this.QUEUE_NAME = QUEUE_NAME;
		this.questCount=questCount;
	}

	@Override
	public void run() {
		try {
			cdl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long startTime = System.currentTimeMillis();
		for (int i = 1000; i < 1000+questCount; i++) {
			System.out.println(Thread.currentThread().getName() + "," + QUEUE_NAME + i + "：" + getQueue(QUEUE_NAME + i));
		}
		System.out.println("-----------执行时间-------------"+(System.currentTimeMillis()-startTime));
	}

	public boolean getQueue(String que) {
		try {
			HttpHost host = new HttpHost("192.168.163.221", 15672);
			HttpGet httpGet = new HttpGet("/api/queues/%2F/" + que);
			BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
			credentialsProvider.setCredentials(new AuthScope("192.168.163.221", 15672),
					new UsernamePasswordCredentials("guest", "guest"));
			CloseableHttpClient client = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider)
					.build();
			HttpResponse response = client.execute(host, httpGet);
			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				String result = EntityUtils.toString(entity);
				JSONObject json = JSONObject.fromObject(result);
				Object obj = json.get("consumers");
				if (Integer.valueOf(obj.toString()) > 0) {
					return true;
				}
			} else {
				System.out.println("状态:" + response.getStatusLine());
			}
		} catch (Exception e) {
			System.out.println("异常：" + e.getMessage());
		}
		return false;
	}
}