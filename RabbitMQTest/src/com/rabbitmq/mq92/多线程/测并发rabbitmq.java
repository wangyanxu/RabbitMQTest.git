package com.rabbitmq.mq92.多线程;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class 测并发rabbitmq {

	public static void main(String[] args) throws IOException {
		String QUEUE_NAME = "task1_more_";
		int threadCount = 1;// 并发数
		int questCount = 2;// 每个线程请求的数量
		CountDownLatch cdl = new CountDownLatch(threadCount);
		for (int i = 0; i < threadCount; i++) {
			new Thread(new MyThreadQueue2(cdl, QUEUE_NAME, questCount)).start();
			cdl.countDown();
		}
	}

}

class MyThreadQueue2 implements Runnable {
	CountDownLatch cdl;
	String QUEUE_NAME;
	int questCount;

	public MyThreadQueue2(CountDownLatch cdl, String QUEUE_NAME, int questCount) {
		this.cdl = cdl;
		this.QUEUE_NAME = QUEUE_NAME;
		this.questCount = questCount;
	}

	@Override
	public void run() {
		try {
			cdl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long startTime = System.currentTimeMillis();
		for (int i = 1000; i < 1000 + questCount; i++) {
			System.out.println(Thread.currentThread().getName() + "," + QUEUE_NAME + i + "：" + getQueue(QUEUE_NAME + i));
		}
		System.out.println("-----------执行时间-------------" + (System.currentTimeMillis() - startTime));
	}

	public boolean getQueue(String que){
		ConnectionFactory factory = new ConnectionFactory();
		// amqp://userName:password@hostName:portNumber/virtualHost
		// System.out.println(channel.getChannelNumber());
		//		System.out.println(channel.messageCount(que));
		long result = 0;
		Connection conn = null;
		Channel channel = null;
		try {
			factory.setUri("amqp://guest:guest@192.168.163.221/%2F");
			conn = factory.newConnection();
			// System.out.println(conn.getChannelMax());
			channel = conn.createChannel();
			result = channel.consumerCount(que);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(channel != null){
				try {
					channel.close();
				} catch (Exception e) {
				}
			}
			if(conn != null){
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
		return result > 0 ? true : false;
	}
}