package com.rabbitmq.mq92.多线程;

import com.rabbitmq.ServerInfo;
import com.rabbitmq.client.*;
import java.io.IOException;

/**
 * 消息消费者
 */
public class 连接 {


	public static void main(String[] argv) throws Exception {
		// 创建连接工厂
		ConnectionFactory factory = new ConnectionFactory();
		// 设置RabbitMQ地址
		factory.setHost(ServerInfo.HOST1);
		// 创建一个新的连接
		Connection connection = factory.newConnection();
		// 创建一个频道
		Channel channel = connection.createChannel();
		String QUEUE_NAME = "task_more_1000";
		channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		System.out.println("C [*] Waiting for messages. To exit press CTRL+C");
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println("C [x] Received '" + message + "'");
			}
		};
		channel.basicConsume(QUEUE_NAME, true, consumer);
	}
}